import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors';


import indexRoutes from './routes/indexRoutes';
import authUserRoutes from './routes/User/authUserRoutes';
import userProfileRouter from './routes/User/userProfileRouter';
import productRoutes from './routes/Product/productRoutes';
import authCategoryRoutes from './routes/Category/authCategoryRoutes';
import shoppingCartRoutes from './routes/ShoppingCart/shoppingCartRoutes';
import favoriteRoutes from './routes/Favorite/favoriteRoutes';

import invoiceRoutes from './routes/Invoice/invoiceRoutes'
class Server {
    
    public app : Application;

    constructor(){
        this.app = express();
        this.config();
        this.routes();
    }

    config():void{
        this.app.set('port', process.env.PORT || 3000);  // puerto en donde escucha
        //this.app.use( morgan('dev') ); //Unicamente para dev
        this.app.use(cors());
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(express.urlencoded({limit: '50mb', extended: true}));

    }

    routes() :void {
        this.app.use(indexRoutes);
        this.app.use('/api/user',authUserRoutes)
        this.app.use('/api/user/profile',userProfileRouter)
        this.app.use('/api/product',productRoutes)
        this.app.use('/api/category',authCategoryRoutes);
        this.app.use('/api/confirmarCompra',shoppingCartRoutes);
        this.app.use('/api/favorite',favoriteRoutes);
        this.app.use('/api/factura',invoiceRoutes);
    }

    start() : void{
        this.app.listen(this.app.get('port'), () =>{
            console.log('Server on port ', this.app.get('port'));
        });
    }

}


const server = new Server();
server.start();