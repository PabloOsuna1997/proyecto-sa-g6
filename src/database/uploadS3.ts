const { s3 } = require('./service3')

function upload (body: any) {
        let i = 0;
        while (true) {
            if (body.imagen[i] == ',') {
                break;
            }
            i++;
        }
        let picture = body.imagen.substring(i + 1, body.imagen.length);
        i = 11
        while (true) {
            if (body.imagen[i] == ';') {
                break;
            }
            i++;
        }
        let type = body.imagen.substring(11, i);
        //picture insertions
        //decodificacion de imagen
        let decode = Buffer.from(picture, 'base64')
        let Name = body.nombre + '.' + type               //cuando el nombre contiene espacios los sustituyo con un + ej:  PRODUCTO DE PRUEBA -> PRODUCTO+DE+PRUEBA

        //creacion de objeto para carga de s3
        let bucket = 'proyecto-sa-g6'

        let upload = {
            Bucket: bucket,
            Key: Name,
            Body: decode,
            ACL: 'public-read'
        }

        //carga de imagen
        s3.upload(upload, function (err: any, data: any) {
            if (err) {
                console.log(err)
            }
        })

        return Name.replace(" ", "+")
    }

export default upload;