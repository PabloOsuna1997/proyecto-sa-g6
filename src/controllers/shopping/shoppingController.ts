import { Request, Response } from "express";
import pool from "../../database/database"; 

class ShoppingController {

    public async registrer(req: Request, res: Response) {
        try {
            var datetime = new Date(); 
            const result = await pool.query(
                "INSERT INTO Factura (fecha, nit, direccion, total, id_cliente) values (?, ?, ?, ?, ?);",
                [datetime,req.body.nit, req.body.direccion, req.body.total, req.body.idCliente]
            ); 
            for (let producto of req.body.productos){ 
                const result2 = await pool.query(
                    "INSERT INTO Detalle_Factura (id_factura, id_producto, cantidad_producto, subtotal) values (?, ?, ?, ?);",
                    [result.insertId,producto.idProducto, producto.cantidad_producto, producto.subtotal]
                    );

                    try{
                        //actualizacion de stock
                        const result3 = await pool.query(
                            "UPDATE Producto SET stock = stock - ? WHERE id_producto = ? AND  (stock - ?) >= 0;",[producto.cantidad_producto, producto.idProducto, producto.cantidad_producto]
                        );
                    }catch(e){
                        console.log("no actualiazo stock")
                    }
                //console.log(result2)
            }
            res.status(200).send({ message: "Factura creada con exito.", numeroFactura: result.insertId});
        } catch (err) {
            console.log(err);
            res.status(500).send({ message: "Ocurrio un error en el sistema." });
        }
    }
}

export const shoppingController = new ShoppingController();
