import { Request, Response } from "express";
import pool from "../../database/database";
import uploadImage from "../../database/uploadS3";

class ProductController {
  public async list(req: Request, res: Response) {
    try {
      const { categoria } = req.params;
      const result = await pool.query(
        "SELECT * FROM Producto WHERE id_categoria = ? AND estado != 0;",
        [categoria]
      );
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async listProductProvider(req: Request, res: Response) {
    try {
      const { id_proveedor } = req.params;
      const result = await pool.query(
        "SELECT p.id_tipo_producto, p.id_producto,p.nombre,p.precio_venta,p.precio_final,p.stock,p.estado,p.id_categoria,p.imagen  FROM Producto p INNER JOIN Lista_Producto lp ON lp.id_producto = p.id_producto WHERE lp.id_proveedor = ? AND p.estado = 1;",
        [id_proveedor]
      );
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  //productos creados por el cliente
  public async listProductClient(req: Request, res: Response) {
    try {
      const { id_cliente } = req.params;
      const result = await pool.query(
        "SELECT p.id_tipo_producto, p.id_producto,p.nombre,p.precio_venta,p.precio_final,p.stock,p.estado,p.id_categoria,p.imagen  FROM Producto p INNER JOIN Lista_Producto_Cliente lp ON lp.id_producto = p.id_producto WHERE lp.id_cliente = ? AND p.estado = 1;",
        [id_cliente]
      );
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  //productos comprados por el cliente
  public async purchasedProductClient(req: Request, res: Response) {
    try {
      const { id_cliente } = req.params;
      const result = await pool.query(
        `SELECT f.fecha, df.cantidad_producto, df.subtotal,p.id_producto,p.nombre,p.precio_venta,p.precio_final,p.stock,p.estado,p.id_categoria,p.imagen 
      FROM Producto p
      INNER JOIN  Detalle_Factura df
      ON df.id_producto = p.id_producto
      INNER JOIN Factura f
      ON f.numero_factura = df.id_factura
      INNER JOIN Cliente c
      ON c.id_cliente = f.id_cliente
      WHERE c.id_cliente = ?;`,
        [id_cliente]
      );
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async clientProductsSales(req: Request, res: Response) {
    try {
      const { id_cliente } = req.params;
      const result = await pool.query(
        `SELECT  p.id_producto, p.nombre, p.precio_final, SUM(df.cantidad_producto) as cantidad
        FROM Producto p
        INNER JOIN  Detalle_Factura df
        ON df.id_producto = p.id_producto
        INNER JOIN Lista_Producto_Cliente lpc
        ON lpc.id_producto = p.id_producto
        INNER JOIN Cliente c
        ON lpc.id_cliente = c.id_cliente
        WHERE c.id_cliente = ?
        GROUP BY  p.id_producto;`,
        [id_cliente]
      );
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async providerProductsSales(req: Request, res: Response) {
    try {
      const { id_proveedor } = req.params;
      const result = await pool.query(
        `SELECT  p.id_producto, p.nombre, p.precio_final, SUM(df.cantidad_producto) as cantidad
        FROM Producto p
        INNER JOIN  Detalle_Factura df
        ON df.id_producto = p.id_producto
        INNER JOIN Lista_Producto lpc
        ON lpc.id_producto = p.id_producto
        INNER JOIN Proveedor c
        ON lpc.id_proveedor = c.id_proveedor
        WHERE c.id_proveedor = ?
        GROUP BY  p.id_producto;`,
        [id_proveedor]
      );
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async registrer(req: Request, res: Response) {
    try {
      if (req.body.imagen[0] != "h") {
        const nameImage = uploadImage(req.body);
        req.body.imagen =
          "https://proyecto-sa-g6.s3.us-east-2.amazonaws.com/" + nameImage;
      }
      const result = await pool.query("INSERT INTO Producto SET ?;", [
        req.body,
      ]);
      res
        .status(200)
        .send({ message: "Producto insertado con exito.", result });
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async provider_product(req: Request, res: Response) {
    try {
      const obj = {
        id_proveedor: req.body.id_proveedor,
        id_producto: req.body.id_producto,
      };

      console.log(obj);
      const result = await pool.query("INSERT INTO Lista_Producto SET ?;", [
        obj,
      ]);
      res.status(200).send({ message: "Listado insertado con exito." });
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }
  public async client_product(req: Request, res: Response) {
    try {
      const obj = {
        id_cliente: req.body.id_cliente,
        id_producto: req.body.id_producto,
      };

      console.log(obj);
      const result = await pool.query(
        "INSERT INTO Lista_Producto_Cliente SET ?;",
        [obj]
      );
      res.status(200).send({ message: "Listado insertado con exito." });
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async update(req: Request, res: Response) {
    try {
      if (req.body.imagen[0] != "h") {
        const nameImage = uploadImage(req.body);
        req.body.imagen =
          "https://proyecto-sa-g6.s3.us-east-2.amazonaws.com/" + nameImage;
      }
      const { id } = req.params;
      const result = await pool.query(
        "UPDATE Producto SET ? WHERE id_producto = ?;",
        [req.body, id]
      );
      res.status(200).send({ message: "Producto editado con exito." });
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async delete(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const result = await pool.query(
        "UPDATE Producto SET estado = 0 WHERE id_producto = ?;",
        [id]
      );
      res
        .status(200)
        .send({ message: "Producto cambiado a estado inhactivo." });
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }

  public async productById(req: Request, res: Response) {
    try {
      const { idProducto } = req.params;
      console.log(idProducto);
      const result = await pool.query(
        "SELECT * FROM Producto WHERE id_producto = ? AND estado != 0;",
        [idProducto]
      );
      console.log(result);
      res.status(200).send(result);
    } catch (err) {
      res.status(500).send({ message: "Ocurrio un error en el sistema." });
    }
  }
}

export const productController = new ProductController();
