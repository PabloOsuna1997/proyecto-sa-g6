import { Request, Response } from 'express';
import pool from '../../database/database';

var nodemailer = require("nodemailer");


class UserAuthController {

    public index(req: Request, res: Response) {
        pool.query('DESCRIBE Proveedor');
        res.send('Hello User')
    }

    public async loginC(req: Request, res: Response) {
        const { mail } = req.params;
        const { pass } = req.params;

        const result = await pool.query('SELECT * FROM Cliente Where correo = ? AND contrasena = ?', [mail, pass]);
        return res.json({ message: result });
    }

    public async loginP(req: Request, res: Response) {
        const { mail } = req.params;
        const { pass } = req.params;

        const result = await pool.query('SELECT * FROM Proveedor WHERE correo = ? AND contrasena = ?', [mail, pass]);
        return res.json({ message: result });
    }

    public async validateMail(req: Request, res: Response) {
        const { mail } = req.params;
        const result = await pool.query('SELECT correo FROM Proveedor WHERE correo = ? UNION ' +
            ' SELECT correo FROM Cliente Where correo = ?', [mail, mail]);

        return res.json({ message: result });
    }

    public async registerProv(req: Request, res: Response) {
        try{
            var prov = await pool.query('INSERT INTO Proveedor set ?', [req.body]);
        res.json({ message: 'Proveedor registrado', data: prov.insertId});
        }catch(error){
            res.json({message:'Proveedor no registrado'})
        }
    }

    public async registerCli(req: Request, res: Response) {
        try {
            const cliente = await pool.query('INSERT INTO Cliente set ?', [req.body]);
            res.json({ message: 'Cliente registrado', data: cliente.insertId });
        }
        catch (error) {
            res.json({ message: 'Cliente no insertado' })
        }
    }

    public async getProveedor(req: Request, res: Response) {
        const { id } = req.params;
        const result = await pool.query('SELECT * FROM Proveedor WHERE id_proveedor = ?', [id]);

        return res.json({ message: result });
    }

    //----------TARJETAS CREDITO
    public async getCreditCards(req: Request, res: Response) {
        const { id } = req.params;
        const result = await pool.query('SELECT * FROM Tarjeta WHERE id_cliente = ?', [id]);

        return res.json({ message: result });
    }

    public async registerCreditCards(req: Request, res: Response) {
        const { numero } = req.body;
        const { fecha_vencimiento } = req.body;
        const { csv } = req.body;
        const { id_tipo } = req.body;
        const { id_cliente } = req.body;

        var splitted = fecha_vencimiento.split("-", 2);
        var mm1 = parseInt(splitted[0]);
        var yy1 = parseInt(splitted[1]) + 2000;

        var today = new Date();
        var mm = parseInt(String(today.getMonth() + 1).padStart(2, '0'));
        var yy = today.getFullYear();
        const fechaFinal = mm + '/' + yy;

        if (yy1 > yy) {
            const result = await pool.query('INSERT INTO ' +
                'Tarjeta(numero,fecha_vencimiento,csv,id_tipo,id_cliente) ' +
                'VALUES(?,?,?,?,?)', [numero, fechaFinal, csv, id_tipo, id_cliente]);
            return res.json({ message: result });
        } else if (yy1 == yy && mm1 >= mm) {
            const result = await pool.query('INSERT INTO ' +
                'Tarjeta(numero,fecha_vencimiento,csv,id_tipo,id_cliente) ' +
                'VALUES(?,?,?,?,?)', [numero, fechaFinal, csv, id_tipo, id_cliente]);
            return res.json({ message: result });
        } else {
            return res.json({ message: 'Fecha de vencimiento invalido' });
        }
    }

    public async findCard(req: Request, res: Response) {
        const { num } = req.params;
        const result = await pool.query('SELECT * FROM Tarjeta WHERE numero = ? ', [num]);

        return res.json({ message: result });
    }

    //mails
    public async sendEmail(req: Request, res: Response) {
        const { destinatario, asunto, mensaje } = req.body;
        console.log("envio correo", req.body);
        var transporte = nodemailer.createTransport({
            service: 'gmail',
            host: 'smtp.gmail.com ',
            port: 465,
            //host: "smtp.ethereal.email",
            //post: 587,
            //secure: false,
            auth: {
                user: "berriosdiego15@gmail.com",
                pass: "Netobran2020!"
            }
        });

        var mailOptions = {
            from: "berriosdiego15@gmail.com",
            to: destinatario,
            subject: asunto,
            text: mensaje
        }
        transporte.sendMail(mailOptions, (error: Error) => {
            if (error) {
                res.status(500).send(error.message);
            } else {
                console.log('email enviado');
                res.status(200).jsonp(req.body);
            }
        });
    }

}

export const userAuthController = new UserAuthController();