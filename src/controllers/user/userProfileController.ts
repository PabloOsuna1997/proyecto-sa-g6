import { Request, Response } from 'express';
import pool from '../../database/database';
import uploadImageUser from "../../database/uploadS3User";


class UserProfileController{

    public async profile(req: Request, res: Response){
        try{
            const { id } = req.params;
            const result = await pool.query('SELECT * FROM Cliente WHERE id_cliente = ?;',[id]);
            res.status(200).send(result);
        }catch(e){
            res.status(500).send({message: 'Ocurrio un error en el sistema.'});
        }
    }

    public async update(req: Request, res: Response){
        try {
            if (req.body.imagen[0] != "h" && req.body.imagen[0] != null) {
                const nameImage = uploadImageUser(req.body);
                req.body.imagen = "https://proyecto-sa-g6.s3.us-east-2.amazonaws.com/clientes/" + nameImage;
            }
            const { id } = req.params;
            const result = await pool.query(
                "UPDATE Cliente SET ? WHERE id_cliente = ?;",
                [req.body, id]
            );
            res.status(200).send({ message: "Cliente editado con exito." });
        } catch (err) {
            res.status(500).send({ message: "Ocurrio un error en el sistema." });
        }
    }
}


export const userProfileController = new UserProfileController();