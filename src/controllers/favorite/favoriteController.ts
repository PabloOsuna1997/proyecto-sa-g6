import {Request , Response} from 'express';
import pool from '../../database/database';

class FavoriteController{

    public async registerFavorite(req: Request, res: Response){
        await pool.query('INSERT INTO Lista_Favoritos set ?', [req.body]);
        res.json({message: 'Favorito registrado'});
    }

    public async listFavorites(req: Request, res: Response){
        const result = await pool.query('SELECT * FROM Producto p INNER JOIN Lista_Favoritos lf ON lf.id_producto=p.id_producto WHERE lf.id_cliente = ?;',[req.body.cliente]);
        res.send(result)
    }

    public async deleteFavorite(req: Request, res: Response){
        await pool.query('DELETE FROM Lista_Favoritos WHERE id_cliente=? AND id_producto=?;', [req.body.cliente,req.body.producto]);
        res.json({message: 'Favorito eliminado'});
    }

}

export const favoriteController = new FavoriteController();