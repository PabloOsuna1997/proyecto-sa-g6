import {Request , Response} from 'express';
import pool from '../../database/database';

class CategoryAuthController{

    public async registerCategory(req: Request, res: Response){
        await pool.query('INSERT INTO Categoria_Producto set ?', [req.body]);
        res.json({message: 'Categoria registrada'});
    }

    public async listCategory(req: Request, res: Response){
        const result = await pool.query('SELECT * FROM Categoria_Producto;');
        res.send(result)
    }

}

export const categoryAuthController = new CategoryAuthController();