show databases;

use project_sa_6;

create table Proveedor(
	id_proveedor 		int not null auto_increment primary key,
    nombre_empresa 		varchar(50) not null,
    correo 				varchar(75) not null,
    contrasena 			varchar(15) not null,
    direccion_fisica 	varchar(100) not null,
    telefono 			int not null
);

create table Categoria_Producto(
	id_categoria 		int not null auto_increment primary key,
    nombre_categoria 	varchar(50) not null
);

create table Producto(
	id_producto			int not null auto_increment primary key,
    nombre 				varchar(50) not null,
    precio_venta		float not null,
    precio_final 		float,
    stock				int not null,
    estado				boolean not null,
    id_categoria 		int not null,
    constraint id_categoria_Producto foreign key (id_categoria) references Categoria_Producto(id_categoria) on delete cascade    
);

/*CREATE TRIGGER after_insert_producto 
    AFTER INSERT ON Producto
    FOR EACH ROW 
    UPDATE Producto SET precio_final = precio_venta + (0.10 * (precio_venta));

drop trigger after_insert_producto;*/

create table Lista_Producto(
	id_proveedor 		int not null,
    id_producto 		int not null,
    constraint  id_proveedor_Lista foreign key (id_proveedor) references Proveedor(id_proveedor) on delete cascade,
    constraint  id_producto_Lista  foreign key (id_producto)  references Producto(id_producto)   on delete cascade
);

create table Tipo_Tarjeta(
	id_tipo_tarjeta 	int not null auto_increment primary key,
    tipo 				varchar(50) not null
);

create table Cliente(
	id_cliente 			int not null auto_increment primary key,
    nombre 				varchar(50) not null,
    apellido			varchar(50) not null,
    correo				varchar(100) not null,
    contrasena			varchar(15) not null,
    celular 			int not null
);

create table Tarjeta(
	id_tarjeta 			int not null auto_increment primary key,
    numero 				long not null, 
    fecha_vencimiento	date,
    csv					int not null,
    id_tipo				int not null,
    id_cliente 			int not null,
    constraint id_tipo_tarjeta foreign key (id_tipo) references Tipo_Tarjeta(id_tipo_tarjeta) on delete cascade,
    constraint id_cliente_tarjeta foreign key (id_cliente) references Cliente(id_cliente) on delete cascade
);

create table Factura(
	numero_factura 		int not null auto_increment primary key,
    fecha				date not null,
    nit 				int not null,
    direccion			varchar(100) not null,
    total				float not null,
    id_cliente			int not null,
    constraint id_cliente_Factura foreign key (id_cliente) references Cliente(id_cliente) on delete cascade
);

create table Detalle_Factura(
	id_factura 			int not null,
    id_producto			int not null,
    cantidad_producto	int not null,
    subtotal			float not null,
    constraint id_factura_detalle foreign key (id_factura) references Factura(numero_factura) on delete cascade,
    constraint id_cliente_detalle foreign key (id_producto) references Cliente(id_cliente) on delete cascade
);
