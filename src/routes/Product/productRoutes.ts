import { Router } from 'express';
import { productController } from '../../controllers/product/productController';

class ProductRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/:categoria', productController.list);
        this.router.get('/provider/:id_proveedor', productController.listProductProvider);                  //productos creados proveedor
        this.router.get('/client/:id_cliente', productController.listProductClient);                        //productos creados cliente
        this.router.get('/getProductsPurchased/:id_cliente', productController.purchasedProductClient);     //productos comprados cliente
        this.router.get('/producto/:idProducto', productController.productById);
        //productos vendidos pos clientes y por proveedores
        this.router.get('/clientProductsSales/:id_cliente', productController.clientProductsSales);
        this.router.get('/providesProductsSales/:id_proveedor', productController.providerProductsSales);

        this.router.post('/', productController.registrer);
        this.router.post('/provider_product', productController.provider_product);
        this.router.post('/client_product', productController.client_product);
        this.router.put('/:id', productController.update);
        this.router.delete('/:id', productController.delete);

        
    }

}

const productRoutes = new ProductRoutes();
export default productRoutes.router;