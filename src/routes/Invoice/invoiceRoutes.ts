import { Router } from 'express';
import { indexController } from '../../controllers/indexController';
import { invoiceController } from '../../controllers/invoice/invoiceController';

class InvoiceRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.post('/generar', invoiceController.list);
        this.router.post('/obtenerDetalleFactura',invoiceController.obtenerDetalleFactura);
        this.router.post('/obtenerEstadoFactura',invoiceController.obtenerEstadoFactura);
        this.router.get('/obtenerDetalleVentas',invoiceController.obtenerVentas);
        this.router.put('/actualizarEstado',invoiceController.cambiarEstado);
        this.router.post('/insertTrack',invoiceController.llenarTracking);
    }

}

const invoiceRoutes = new InvoiceRoutes();
export default invoiceRoutes.router;