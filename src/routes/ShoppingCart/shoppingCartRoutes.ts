import { Router } from 'express';
import { shoppingController } from '../../controllers/shopping/shoppingController';

class ShoppingCartRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.post('/', shoppingController.registrer);
    }

}

const shoppingCartRoutes = new ShoppingCartRoutes();
export default shoppingCartRoutes.router;