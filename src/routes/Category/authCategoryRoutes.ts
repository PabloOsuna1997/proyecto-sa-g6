import { Router } from 'express';
import { categoryAuthController } from '../../controllers/category/categoryAuthController'

class AuthCategoryRoutes{

    public router : Router = Router();
    
    constructor(){
        this.config();
    }

    config():void{
        this.router.post('/registerCategory',categoryAuthController.registerCategory);
        this.router.get('/list',categoryAuthController.listCategory);
    }

}

const authCategoryRoutes = new AuthCategoryRoutes();
export default authCategoryRoutes.router;