import { Router } from 'express';
import { userProfileController } from '../../controllers/user/userProfileController';

class UserProfileRouter{

    public router: Router = Router();

    constructor(){
        this.config();
    }

    private config(){
        this.router.get('/:id', userProfileController.profile);
        this.router.put('/:id', userProfileController.update);
    }
}


const userProfileRouter = new UserProfileRouter();
export default userProfileRouter.router;