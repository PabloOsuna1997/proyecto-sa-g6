import { Router } from 'express';
import { userAuthController } from '../../controllers/user/userAuthController'

class AuthUserRoutes{

    public router : Router = Router();
    
    constructor(){
        this.config();
    }

    config():void{
        this.router.get('/',userAuthController.index);
        this.router.post('/registerProv',userAuthController.registerProv);
        this.router.post('/registerCli',userAuthController.registerCli);
        this.router.get('/loginC/:mail/:pass',userAuthController.loginC);
        this.router.get('/loginP/:mail/:pass',userAuthController.loginP);
        this.router.get('/validateMail/:mail',userAuthController.validateMail);
        this.router.get('/getProveedor/:id',userAuthController.getProveedor);
        //Credit Cards 
        this.router.post('/registerCreditCard',userAuthController.registerCreditCards);
        this.router.get('/getCreditCards/:id',userAuthController.getCreditCards);
        this.router.get('/getCreditCard/:num',userAuthController.findCard);
        
        this.router.post('/send-email',userAuthController.sendEmail);


    }

}

const authUserRoutes = new AuthUserRoutes();
export default authUserRoutes.router;