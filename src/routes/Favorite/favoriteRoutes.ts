import { Router } from 'express';
import { favoriteController } from '../../controllers/favorite/favoriteController'

class FavoriteRoutes{

    public router : Router = Router();
    
    constructor(){
        this.config();
    }

    config():void{
        this.router.post('/registerFavorite',favoriteController.registerFavorite);
        this.router.post('/listFavorites',favoriteController.listFavorites);
        this.router.post('/deleteFavorite',favoriteController.deleteFavorite);
    }

}

const favoriteRoutes = new FavoriteRoutes();
export default favoriteRoutes.router;