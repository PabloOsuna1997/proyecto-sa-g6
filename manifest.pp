package { 'docker.io':
    ensure => 'installed'
}

exec { 'clean':
    command => '/usr/bin/rm -rf proyecto-sa-g6',
    onlyif => '/bin/ls proyecto-sa-g6',
    logoutput => true,
    before => Exec['clone']
}

exec { 'clone':
    command => '/usr/bin/git clone https://gitlab.com/PabloOsuna1997/proyecto-sa-g6.git',
    unless => "/bin/ls proyecto-sa-g6",
    require => Package['docker.io'],
    logoutput => true
}

exec { 'build':
    command => '/usr/bin/docker build -t servicemain ./proyecto-sa-g6/',
    require => Exec['clone'],
    logoutput => true
}

exec { 'preparation':
    command => '/usr/bin/docker rm -f servicemain',
    require => Exec['build'],
    logoutput => true
}

exec { 'run':
    command => '/usr/bin/docker run -d --name servicemain -p 3000:3000 servicemain',
    require => Exec['preparation'],
    logoutput => true
}