var assert    = require("chai").assert;
var chai    = require("chai");
var chaiHttp = require('chai-http');
chai.use(chaiHttp);
const url= 'http://localhost:3000';
const req = { body: { nombre_categoria: "categoria nueva" }}
const expect = require('chai').expect;

describe('Insert a category:  ',()=>{
    it('should insert a category', (done) => {
    chai.request(url)
    .post('/api/category/registerCategory')
    .send({nombre_categoria: "categoria nueva"})
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });
