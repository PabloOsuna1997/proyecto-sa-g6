var chai    = require("chai");
var chaiHttp = require('chai-http');
chai.use(chaiHttp);
const url= 'http://localhost:3000';
const expect = require('chai').expect;

describe('List a product: ',()=>{
    it('should list a product', (done) => {
    chai.request(url)
    .get('/api/product/1')
    .end( function(err,res){
    console.log(res.body)
    expect(res).to.have.status(200);
    done();
    });
    });
   });
